package ru.t1.semikolenov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.t1.semikolenov.tm.model.Project;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ProjectRepository {

    @NotNull
    private final Map<String, Project> projects = new HashMap<>();

    {
        add("project 1");
        add("project 2");
        add("project 3");
    }

    public Project add(@NotNull final String name) {
        final Project project = new Project(name);
        return projects.put(project.getId(), project);
    }

    public Project add(@NotNull final Project project) {
        return projects.put(project.getId(), project);
    }

    public void removeById(@NotNull final String id) {
        projects.remove(id);
    }

    @NotNull
    public List<Project> findAll() {
        return new ArrayList<>(projects.values());
    }

    @NotNull
    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    public boolean existsById(@NotNull final String id) {
        return projects.containsKey(id);
    }

    public long count() {
        return projects.size();
    }

    public Project save(@NotNull final Project project) {
        return projects.put(project.getId(), project);
    }

    public void remove(@NotNull final Project project) {
        projects.remove(project.getId());
    }

    public void remove(@NotNull final List<Project> projects) {
        projects.forEach(this::remove);
    }

    public void clear() {
        projects.clear();
    }

}
