package ru.t1.semikolenov.tm.configuration;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.t1.semikolenov.tm")
public class ApplicationConfiguration {
}
