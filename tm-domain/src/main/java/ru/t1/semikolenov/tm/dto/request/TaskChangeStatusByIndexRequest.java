package ru.t1.semikolenov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class TaskChangeStatusByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    @Nullable
    private Status status;

    public TaskChangeStatusByIndexRequest(
            @Nullable final String token,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        super(token);
        this.index = index;
        this.status = status;
    }

}