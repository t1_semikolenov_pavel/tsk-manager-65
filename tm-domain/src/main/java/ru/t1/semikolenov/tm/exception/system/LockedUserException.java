package ru.t1.semikolenov.tm.exception.system;

import ru.t1.semikolenov.tm.exception.AbstractException;

public class LockedUserException extends AbstractException {

    public LockedUserException() {
        super("Error! User is locked...");
    }

}